import { Injectable } from '@angular/core';
import { Http } from '@Utils/class/http.class';
import { API_BASE_URL } from '@Resources/constants/url.constant';
import { map } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';
import { CountryType } from '@Resources/types/country.type';
import { ApiResponse } from '@Resources/types/http.type';
import { LocalDataService } from '@services/localData/local-data.service';

@Injectable({
	providedIn: 'root',
})
export class CountryService {
	constructor(private http: Http, private localData: LocalDataService) {}

	public getCountries(): Observable<CountryType[]> {
		this.http.apiUrl = API_BASE_URL;
		return this.http.get<ApiResponse<CountryType[]>>('all').pipe(
			map(value => {
				const parsedData: CountryType[] = (value.body as unknown) as CountryType[];
				parsedData.map(value1 => {
					const aux: string[] = [];
					for (const b of value1.borders) {
						const country: CountryType = parsedData.find(value2 => value2.alpha3Code === b);
						if (country) aux.push(country.name);
					}
					value1.fullBorders = aux;
				});
				this.localData.setCountryInfo(parsedData);
				return parsedData;
			})
		);
	}
}
