import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CountryType } from '@Resources/types/country.type';

@Injectable({
	providedIn: 'root',
})
export class LocalDataService {
	private countryInfoBs = new BehaviorSubject<CountryType[]>([]);
	public countryInfo$ = this.countryInfoBs.asObservable();
	constructor() {}

	setCountryInfo(countryInfo: CountryType[]) {
		this.countryInfoBs.next(countryInfo);
	}
}
