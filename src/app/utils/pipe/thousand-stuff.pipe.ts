import { Pipe, PipeTransform } from '@angular/core';
import { ContinentType, CountryType } from '@Resources/types/country.type';

@Pipe({
	name: 'thousandStuff',
})
export class ThousandStuffPipe implements PipeTransform {
	transform(input: any, args?: any): any {
		let exp;
		const suffixes = ['k', 'M', 'G', 'T', 'P', 'E'];

		if (Number.isNaN(input)) {
			return null;
		}

		if (input < 1000) {
			return input;
		}
		exp = Math.floor(Math.log(input) / Math.log(1000));

		return (input / Math.pow(1000, exp)).toFixed(args) + suffixes[exp - 1];
	}
}

@Pipe({
	name: 'filterCountryKey',
})
export class KeyFilter implements PipeTransform {
	transform(value: CountryType[], args?: any): any {
		const regions: ContinentType[] = [];
		value.forEach(val => {
			if (regions.length === 0) regions.push({ enName: val[args], esName: val[args] });
			if (!regions.filter(value1 => value1.enName === val[args]).length) regions.push({ enName: val[args], esName: val[args] });
		});
		return regions.filter(v => v.enName !== '').sort((a, b) => (a.enName > b.enName ? 1 : b.enName > a.enName ? -1 : 0));
	}
}

@Pipe({
	name: 'filterCountry',
})
export class FilterCountry implements PipeTransform {
	transform(value: CountryType[], args: string): any {
		return value.filter(value1 => value1.region === args);
	}
}
