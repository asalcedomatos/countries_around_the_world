import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StoreFavorites {
	public addToFavorites(favorite: string): void {
		let favorites: string[] = JSON.parse(localStorage.getItem('favorites'));
		if (!favorites) favorites = [];
		favorites.push(favorite);
		localStorage.setItem('favorites', JSON.stringify(favorites));
	}

	public removeFromFavorites(favorite: string): void {
		let favorites: string[] = JSON.parse(localStorage.getItem('favorites'));
		if (favorites) {
			favorites = favorites.filter(value => value !== favorite);
			localStorage.setItem('favorites', JSON.stringify(favorites));
		}
	}

	public checkFavorite(favorite: string): boolean {
		const favorites: string[] = JSON.parse(localStorage.getItem('favorites'));
		if (!favorites?.length) return false;
		return favorites.filter(value => value === favorite).length > 0;
	}
}
