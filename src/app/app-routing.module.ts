import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HandleCountryComponent } from '@Smarts/handle-country/handle-country.component';

const routes: Routes = [
	{
		path: '',
		component: HandleCountryComponent,
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})
export class AppRoutingModule {}
