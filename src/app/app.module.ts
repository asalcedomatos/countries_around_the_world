import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainViewComponent } from './main-view/main-view.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HandleCountryComponent } from '@Smarts/handle-country/handle-country.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { HttpClientModule } from '@angular/common/http';
import { CountrySimpleUIComponent } from '@Dummies/country-simple-ui/country-simple-ui.component';
import { ContinentGroupComponent } from '@Dummies/continent-group/continent-group.component';
import { DialogCountryInfoComponent } from '@Dummies/dialog-country-info/dialog-country-info.component';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { FilterCountry, KeyFilter, ThousandStuffPipe } from '@Utils/pipe/thousand-stuff.pipe';

@NgModule({
	declarations: [
		AppComponent,
		MainViewComponent,
		HandleCountryComponent,
		CountrySimpleUIComponent,
		ContinentGroupComponent,
		DialogCountryInfoComponent,
		ThousandStuffPipe,
		KeyFilter,
		FilterCountry,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		LayoutModule,
		MatToolbarModule,
		MatButtonModule,
		MatSidenavModule,
		MatIconModule,
		MatListModule,
		MatFormFieldModule,
		MatInputModule,
		HttpClientModule,
		MatDialogModule,
		FormsModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
