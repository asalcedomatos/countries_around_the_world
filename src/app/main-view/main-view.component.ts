import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, Subscription } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import Fuse from 'fuse.js';
import { LocalDataService } from '@services/localData/local-data.service';
import { CountryType } from '@Resources/types/country.type';

@Component({
	selector: 'app-main-view',
	templateUrl: './main-view.component.html',
	styleUrls: ['./main-view.component.scss'],
})
export class MainViewComponent {
	isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
		map(result => result.matches),
		shareReplay()
	);

	public searchWord: string;
	private rawCountryList: CountryType[];
	private countryArray: Subscription;

	options = {
		shouldSort: true,
		threshold: 0.4,
		location: 0,
		distance: 5,
		maxPatternLength: 32,
		minMatchCharLength: 4,
		findAllMatches: true,
		keys: ['name'],
	};

	constructor(private breakpointObserver: BreakpointObserver, private localData: LocalDataService) {}

	onFormSubmit(event) {
		event.preventDefault();
	}

	public onChange(event: any) {
		if (!this.rawCountryList)
			this.countryArray = this.localData.countryInfo$.subscribe(value => {
				if (!this.rawCountryList) this.rawCountryList = value;
			});
		else this.countryArray.unsubscribe();

		const fuse = new Fuse(this.rawCountryList, this.options);
		const result: CountryType[] = event === '' ? this.rawCountryList : fuse.search(event).map(value => value.item);
		this.localData.setCountryInfo(result);
	}
}
