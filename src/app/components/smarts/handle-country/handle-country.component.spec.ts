import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandleCountryComponent } from './handle-country.component';

describe('HandleCountryComponent', () => {
	let component: HandleCountryComponent;
	let fixture: ComponentFixture<HandleCountryComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [HandleCountryComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(HandleCountryComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
