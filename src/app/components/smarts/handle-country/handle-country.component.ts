/* tslint:disable:component-class-suffix no-string-literal */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { CountryService } from '@services/country/country.service';
import { Subscription } from 'rxjs';
import { LocalDataService } from '@services/localData/local-data.service';
import { CountryType } from '@Resources/types/country.type';
import { MatDialog } from '@angular/material/dialog';
import { DialogCountryInfoComponent } from '@Dummies/dialog-country-info/dialog-country-info.component';

@Component({
	selector: 'app-handle-country',
	templateUrl: './handle-country.component.html',
	styleUrls: ['./handle-country.component.scss'],
})
export class HandleCountryComponent implements OnInit, OnDestroy {
	public countriesSubscription: Subscription;
	constructor(private conuntryService: CountryService, public localDataService: LocalDataService, public dialog: MatDialog) {}

	onSelectedCountry(event: CountryType): void {
		this.dialog.open(DialogCountryInfoComponent, {
			width: '550px',
			data: event,
		});
	}

	ngOnInit(): void {
		this.countriesSubscription = this.conuntryService.getCountries().subscribe();
	}

	ngOnDestroy(): void {
		if (this.countriesSubscription) this.countriesSubscription.unsubscribe();
	}
}
