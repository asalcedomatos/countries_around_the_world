/* tslint:disable:no-input-rename */
import { Component, Input, OnInit } from '@angular/core';
import { CountryType } from '@Resources/types/country.type';
import { StoreFavorites } from '@Utils/class/store-auth.class';

@Component({
	selector: 'app-country-simple-ui',
	templateUrl: './country-simple-ui.component.html',
	styleUrls: ['./country-simple-ui.component.scss'],
})
export class CountrySimpleUIComponent implements OnInit {
	@Input('country') data: CountryType;

	constructor(public storeFavorites: StoreFavorites) {}

	ngOnInit(): void {}
}
