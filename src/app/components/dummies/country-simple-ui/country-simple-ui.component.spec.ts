import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySimpleUIComponent } from './country-simple-ui.component';

describe('CountrySimpleUIComponent', () => {
	let component: CountrySimpleUIComponent;
	let fixture: ComponentFixture<CountrySimpleUIComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [CountrySimpleUIComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CountrySimpleUIComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
