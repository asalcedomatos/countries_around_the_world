import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContinentGroupComponent } from './continent-group.component';

describe('ContinentGroupComponent', () => {
	let component: ContinentGroupComponent;
	let fixture: ComponentFixture<ContinentGroupComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [ContinentGroupComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ContinentGroupComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
