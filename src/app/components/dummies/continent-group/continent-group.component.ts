/* tslint:disable:no-input-rename */
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ContinentType, CountryType } from '@Resources/types/country.type';

@Component({
	selector: 'app-continent-group',
	templateUrl: './continent-group.component.html',
	styleUrls: ['./continent-group.component.scss'],
})
export class ContinentGroupComponent implements OnInit {
	@Input('countryList') list: CountryType[];
	@Input('continent') continent: ContinentType;
	@Output('onCountrySelected') selected = new EventEmitter<CountryType>();

	constructor() {}

	ngOnInit(): void {}
}
