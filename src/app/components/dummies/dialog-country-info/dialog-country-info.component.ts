import { Component, Inject, OnInit, EventEmitter } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CountryType } from '@Resources/types/country.type';
import { StoreFavorites } from '@Utils/class/store-auth.class';

@Component({
	selector: 'app-dialog-country-info',
	templateUrl: './dialog-country-info.component.html',
	styleUrls: ['./dialog-country-info.component.scss'],
})
export class DialogCountryInfoComponent implements OnInit {
	public favorite = false;
	constructor(
		public dialogRef: MatDialogRef<DialogCountryInfoComponent>,
		@Inject(MAT_DIALOG_DATA) public country: CountryType,
		private storeFavorites: StoreFavorites
	) {}

	addFavorite() {
		if (!this.storeFavorites.checkFavorite(this.country.name)) this.storeFavorites.addToFavorites(this.country.name);
		this.favorite = this.storeFavorites.checkFavorite(this.country.name);
	}

	removeFavorite() {
		if (this.storeFavorites.checkFavorite(this.country.name)) this.storeFavorites.removeFromFavorites(this.country.name);
		this.favorite = this.storeFavorites.checkFavorite(this.country.name);
	}

	ngOnInit(): void {
		this.favorite = this.storeFavorites.checkFavorite(this.country.name);
	}
}
