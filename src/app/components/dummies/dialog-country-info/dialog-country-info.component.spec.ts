import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogCountryInfoComponent } from './dialog-country-info.component';

describe('DialogCountryInfoComponent', () => {
	let component: DialogCountryInfoComponent;
	let fixture: ComponentFixture<DialogCountryInfoComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [DialogCountryInfoComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(DialogCountryInfoComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
