import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';

export const APP_NAME = 'Test Qvey';
export const API_BASE_URL = 'https://restcountries.eu/rest/v2/';
export const MESSAGE_CLIENT_ERROR_HTTP = (message: HttpErrorResponse) => `An error occurred:, ${message.message}`;
export const MESSAGE_SERVE_ERROR_HTTP = (error: HttpErrorResponse) => `Backend return code ${error.status} body was: ${error.message}`;
export const API_PAYMENT_ACCOUNT = '/account';
