export interface CountryType {
	name: string;
	alpha2Code: string;
	alpha3Code: string;
	capital: string;
	region: string;
	subregion: string;
	population: number;
	demonym: string;
	area: number;
	gini: string;
	nativeName: string;
	flag: string;
	cioc: string;
	topLevelDomain: string[];
	callingCodes: string[];
	altSpellings: string[];
	latlng: number[];
	timezones: string[];
	borders: string[];
	currencies: CurrencyType[];
	languages: LanguageType[];
	translations: TranslationType[];
	regionalBlocs: RegionalBlockType[];
	fullBorders: string[];
}

export interface CurrencyType {
	code: string;
	name: string;
	symbol: string;
}

export interface LanguageType {
	iso639_1: string;
	iso639_2: string;
	name: string;
	nativeName: string;
}

export interface TranslationType {
	de: string;
	es: string;
	fr: string;
	ja: string;
	it: string;
	br: string;
	pt: string;
	nl: string;
	hr: string;
	fa: string;
}

export interface RegionalBlockType {
	acronym: string;
	name: string;
	otherAcronyms: string[];
	otherNames: string[];
}

export interface ContinentType {
	esName: string;
	enName: string;
}
